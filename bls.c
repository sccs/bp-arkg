#include <pbc.h>
#include <sodium.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define KDF1 "1"
#define KDF2 "2"
#define KDF_LEN 16

int main(void) {
   clock_t start_t, end_t;
   double total_t;
    if (sodium_init() < 0) {
        return 1;
    }

    pairing_t pairing;
    char param[1024];
    size_t count = fread(param, 1, 1024, stdin);
    if (!count) pbc_die("input error");
    pairing_init_set_buf(pairing, param, count);
    
  uint8_t dpk_kcred[KDF_LEN], dpk_kmac[KDF_LEN], dsk_kcred[KDF_LEN], dsk_kmac[KDF_LEN];
  unsigned char kdf_out_hex[sizeof(uint8_t) * KDF_LEN * 2 + 1];
  unsigned char dpk_phi_bytes[64], dsk_phi_bytes[64];
    
  element_t g, h;
  element_t pk, pke, pkp, pkp_rec;
  element_t sk, ske, skp;
  element_t dpk_phi, dpk_kcred_g, dsk_phi, dsk_kcred_g;
  
  element_init_G2(g, pairing);
  element_init_G2(pk, pairing);
  element_init_G2(pke, pairing);
  element_init_G2(pkp, pairing);
  element_init_G2(pkp_rec, pairing);
  element_init_Zr(sk, pairing);
  element_init_Zr(ske, pairing);
  element_init_Zr(skp, pairing);
  element_init_GT(dpk_phi, pairing);
  element_init_GT(dsk_phi, pairing);
  element_init_Zr(dpk_kcred_g, pairing);
  element_init_Zr(dsk_kcred_g, pairing);
  
  //printf("BLS compatible keys\n");

  element_random(g);
  //element_printf("pp.g = %B\n", g);
  start_t = clock();
  element_random(sk);
  element_pow_zn(pk, g, sk);
  //element_printf("sk = %B\n", sk);
  //element_printf("pk = %B\n", pk);
    end_t = clock();
  total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
  printf("kgen %f\n", total_t  );
 //printf("dpk(pk)\n");
  element_random(ske);
  element_pow_zn(pke, g, sk);
  //element_printf("ske = %B\n", ske);
  //element_printf("pke = %B\n", pke);
  
  element_pairing(dpk_phi, ske, pk);
  //element_printf("dpk.phi(ske, pk) = %B\n", dpk_phi);
  
  element_to_bytes(dpk_phi_bytes, dpk_phi);
  crypto_kdf_derive_from_key(dpk_kcred, sizeof dpk_kcred, 1, KDF1, dpk_phi_bytes);
  sodium_bin2hex(kdf_out_hex, sizeof kdf_out_hex, dpk_kcred, sizeof dpk_kcred);
 //printf("dpk.kcred = %s\n", kdf_out_hex);

  crypto_kdf_derive_from_key(dpk_kmac, sizeof dpk_kmac, 2, KDF2, (const unsigned char *) dpk_phi);/*
  sodium_bin2hex(kdf_out_hex, sizeof kdf_out_hex, dpk_kmac, sizeof dpk_kmac);
 //printf("dpk.kmac = %s\n", kdf_out_hex);*/
  
  element_from_hash(dpk_kcred_g, dpk_kcred, sizeof dpk_kcred);
  element_pow_zn(pkp, g, dpk_kcred_g);
  element_add(pkp, pkp, pk);
  //element_printf("pkp = %B\n", pkp);
    end_t = clock();
  total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
  printf("dpk %f\n", total_t  );
 //printf("Skipping MAC for now\n");
    start_t = clock();
 //printf("dsk(sk,pke)\n"); 
  element_pairing(dsk_phi, sk, pke);
  //element_printf("dsk.phi(sk, pke) = %B\n", dsk_phi);
  
  element_to_bytes(dsk_phi_bytes, dsk_phi);
  crypto_kdf_derive_from_key(dsk_kcred, sizeof dsk_kcred, 1, KDF1, dsk_phi_bytes);
  sodium_bin2hex(kdf_out_hex, sizeof kdf_out_hex, dsk_kcred, sizeof dsk_kcred);
 //printf("dsk.kcred = %s\n", kdf_out_hex);

  

  crypto_kdf_derive_from_key(dpk_kmac, sizeof dpk_kmac, 2, KDF2, (const unsigned char *) dpk_phi);/*
  sodium_bin2hex(kdf_out_hex, sizeof kdf_out_hex, dpk_kcred, sizeof dpk_kcred);
 //printf("dpk.kmac = %s\n", kdf_out_hex);*/
  
  element_from_hash(dsk_kcred_g, dsk_kcred, sizeof dsk_kcred);
  element_add(skp, dsk_kcred_g, sk);
  //element_printf("skp = %B\n", skp);
    end_t = clock();
  total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
  printf("dsk %f\n", total_t  );
    start_t = clock();
  element_pow_zn(pkp_rec, g, skp);
  //element_printf("pkp = %B\n", pkp);
  //element_printf("pkp_rec = %B\n", pkp_rec);
 //printf("%i\n", element_cmp(pkp_rec, pkp));
    end_t = clock();
  total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
  printf("check %f\n", total_t  );
return 0;

}
