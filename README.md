# ARKG using bilinear pairing schemes

*This implementation of BP-ARKG is not designed for use in production and comes with no warranty.*

## Building

The Type-1 pairings use libpbc and libsodium, so build and link as:

    gcc [bls|cl].c -lpbc -lgmp -lsodium

And run with Type-1 parameters for libpbc:
    ./a.out < param/a.param

The Type-2/3 schemes use the bplib and cryptography Python libraries. Tested with Python 3.9.
    python3.9 [sps|waters|ps].py
