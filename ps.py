from collections import namedtuple
from bplib.bp import BpGroup
from petlib.bn import Bn
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.hmac import HMAC
from cryptography.hazmat.primitives.hashes import SHA256
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from time import time

Params = namedtuple('Params', 'phi G g1 g2 o mac kdf_1 kdf_2')
Cred = namedtuple('Cred', 'pke mu aux')
Sk = namedtuple('Sk', 'x1 x2')
Pk = namedtuple('Pk', 'y1 y2')


def _hmac(k, m):
    hmac = HMAC(k, SHA256(), default_backend())
    hmac.update(m)
    return hmac.finalize()


def _hkdf(k, l):
    hkdf = HKDF(
        algorithm=SHA256(),
        length=64,
        salt=None,
        info=l,
        backend=default_backend())
    return hkdf.derive(k)


def setup():
    G = BpGroup()
    g1, g2 = G.gen1(), G.gen2()
    kdf_1 = lambda k: _hkdf(k, b'1') 
    kdf_2 = lambda k: _hkdf(k, b'2')
    phi = lambda a, b: G.pair(b.y1, g2*a.x1) * G.pair(b.y2, g2*a.x2)
    return Params(phi, G, g1, g2, G.order(), _hmac, kdf_1, kdf_2)


def kgen(pp):
    x1, x2 = pp.o.random(), pp.o.random()
    return Sk(x1, x2), Pk(pp.g1*x1, pp.g1*x2)


def check(pp, sk, pk):
    return (pp.g1*sk.x1, pp.g1*sk.x2) == pk


def derive_pk(pp, pk, aux):
    ske, pke = kgen(pp)

    phi_eval = pp.phi(ske, pk).export()
    ck_bytes = pp.kdf_1(phi_eval)
    mk_bytes = pp.kdf_2(phi_eval)
    ck = Bn.from_binary(ck_bytes)
    
    pkp = Pk(pp.g1*ck + pk.y1, pp.g1*ck + pk.y2)
    mu = pp.mac(mk_bytes, pke.y1.export() + pke.y2.export() + aux)
    
    return pkp, Cred(pke, mu, aux)


def derive_sk(pp, sk, cred):
    phi_eval = pp.phi(sk, cred.pke).export()
    mk_bytes = pp.kdf_2(phi_eval)
    mu = pp.mac(mk_bytes, cred.pke.y1.export() + cred.pke.y2.export() + cred.aux)
    
    if mu == cred.mu:
        ck_bytes = pp.kdf_1(phi_eval)
        ck = Bn.from_binary(ck_bytes)
        return Sk(sk.x1+ck, sk.x2+ck)
    
    return False


if __name__ == '__main__':
    pp = setup()
    t1 = time()
    sk, pk = kgen(pp)
    print('kgen', time() - t1)
    t1 = time()
    pkp, cred = derive_pk(pp, pk, b'')
    print('dpk', time() - t1)
    t1 = time()
    skp = derive_sk(pp, sk, cred)
    print('dsk', time() - t1)
    
    assert check(pp, sk, pk)
    t1 = time()
    assert check(pp, skp, pkp)
    print('check', time() - t1)
    assert pk != pkp and sk != skp
    assert pp.phi(skp, pk) == pp.phi(sk, pkp)
    
    print('Suceeded.')
