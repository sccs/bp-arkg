from collections import namedtuple
from bplib.bp import BpGroup
from petlib.bn import Bn
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.hmac import HMAC
from cryptography.hazmat.primitives.hashes import SHA256
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from time import time
Params = namedtuple('Params', 'phi G g1 g2 o mac kdf_1 kdf_2 l')
Cred = namedtuple('Cred', 'pke mu aux')
Sk = namedtuple('Sk', 'gx1 x2 x3 x4 x5')
Pk = namedtuple('Pk', 'gtx1 y2 y3 y4 y5')


def _hmac(k, m):
    hmac = HMAC(k, SHA256(), default_backend())
    hmac.update(m)
    return hmac.finalize()


def _hkdf(k, l):
    hkdf = HKDF(
        algorithm=SHA256(),
        length=64,
        salt=None,
        info=l,
        backend=default_backend())
    return hkdf.derive(k)


def setup():
    G = BpGroup()
    g1, g2 = G.gen1(), G.gen2()
    kdf_1 = lambda k: _hkdf(k, b'1') 
    kdf_2 = lambda k: _hkdf(k, b'2')
    phi = lambda a, b: G.pair(a[0], pp.g2)*b[0]*G.pair(g1*a[1], b[1])*G.pair(g1*a[2], b[2])*G.pair(g1*a[3], b[3])*G.pair(g1*a[4], b[4])
    return Params(phi, G, g1, g2, G.order(), _hmac, kdf_1, kdf_2, 5)


def kgen(pp):
    sk = [pp.o.random() for _ in range(pp.l)]
    pk = [pp.g2*i for i in sk]
    sk[0] = pp.g1*sk[0]
    pk[0] = pp.G.pair(sk[0], pp.g2)

    return sk, pk


def check(pp, sk, pk):
    pk_rec = [pp.G.pair(sk[0], pp.g2)]
    pk_rec.extend([pp.g2*i for i in sk[1:]])
    return pk_rec == pk


def derive_pk(pp, pk, aux):
    ske, pke = kgen(pp)

    phi_eval = pp.phi(ske, pk).export()
    ck_bytes = pp.kdf_1(phi_eval)
    mk_bytes = pp.kdf_2(phi_eval)

    ck = Bn.from_binary(ck_bytes)
    
    pkp = [pk[0] * pp.G.pair(pp.g1*ck, pp.g2)]
    pkp.extend([pp.g2*ck+i for i in pk[1:]])
    
    mu = pp.mac(mk_bytes, pke[0].export() + pke[1].export() + pke[2].export() + pke[3].export() + pke[4].export() + aux)

    return pkp, Cred(pke, mu, aux)


def derive_sk(pp, sk, cred):
    phi_eval = pp.phi(sk, cred.pke).export()
    mk_bytes = pp.kdf_2(phi_eval)

    mu = pp.mac(mk_bytes, cred.pke[0].export() + cred.pke[1].export() + cred.pke[2].export() + cred.pke[3].export() + cred.pke[4].export() + cred.aux)

    if mu == cred.mu:
        ck_bytes = pp.kdf_1(phi_eval)
        ck = Bn.from_binary(ck_bytes)
   
        skp = [sk[0] + pp.g1*ck]
        skp.extend([i+ck for i in sk[1:]])
        return skp
    
    return False


if __name__ == '__main__':
    pp = setup()
    t1 = time()
    sk, pk = kgen(pp)
    print('kgen', time() - t1)
    t1 = time()
    pkp, cred = derive_pk(pp, pk, b'')
    print('dpk', time() - t1)
    t1 = time()
    skp = derive_sk(pp, sk, cred)
    print('dsk', time() - t1)
    
    assert check(pp, sk, pk)
    t1 = time()
    assert check(pp, skp, pkp)
    print('check', time() - t1)
    #assert pk != pkp and sk != skp
    #assert pp.phi(skp, pk) == pp.phi(sk, pkp)
    
    print('Suceeded.')
