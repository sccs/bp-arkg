#include <pbc.h>
#include <sodium.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define KDF1 "1"
#define KDF2 "2"
#define KDF_LEN 16

int main(void) {
   clock_t start_t, end_t;
   double total_t;

    if (sodium_init() < 0) {
        return 1;
    }

    pairing_t pairing;
    char param[1024];
    size_t count = fread(param, 1, 1024, stdin);
    if (!count) pbc_die("input error");
    pairing_init_set_buf(pairing, param, count);
    
  uint8_t dpk_kcred[KDF_LEN], dpk_kmac[KDF_LEN], dsk_kcred[KDF_LEN], dsk_kmac[KDF_LEN];
  unsigned char kdf_out_hex[sizeof(uint8_t) * KDF_LEN * 2 + 1];
  unsigned char dpk_phi_bytes[64], dsk_phi_bytes[64];
    
  element_t g;
  element_t pk_a, pk_b, pke_a, pke_b, pkp_a, pkp_b, pkp_rec_a, pkp_rec_b;
  element_t sk_a, sk_b, ske_a, ske_b, skp_a, skp_b;
  element_t dpk_phi, dpk_kcred_g, dsk_phi, dsk_kcred_g;
  
  element_init_G2(g, pairing);
  element_init_G2(pk_a, pairing);
  element_init_G2(pk_b, pairing);
  element_init_G2(pke_a, pairing);
  element_init_G2(pke_b, pairing);
  element_init_G2(pkp_a, pairing);
  element_init_G2(pkp_b, pairing);
  element_init_G2(pkp_rec_a, pairing);
  element_init_G2(pkp_rec_b, pairing);
  element_init_Zr(sk_a, pairing);
  element_init_Zr(sk_b, pairing);
  element_init_Zr(ske_a, pairing);
  element_init_Zr(ske_b, pairing);
  element_init_Zr(skp_a, pairing);
  element_init_Zr(skp_b, pairing);
  element_init_GT(dpk_phi, pairing);
  element_init_GT(dsk_phi, pairing);
  element_init_Zr(dpk_kcred_g, pairing);
  element_init_Zr(dsk_kcred_g, pairing);
  
  //printf("CL compatible keys\n");

  element_random(g);
  //element_printf("pp.g = %B\n", g);
  start_t = clock();
  element_random(sk_a);
  element_random(sk_b);
  element_pow_zn(pk_a, g, sk_a);
  element_pow_zn(pk_b, g, sk_b);
  //element_printf("sk_a = %B\n", sk_a);
  //element_printf("sk_b = %B\n", sk_b);
  //element_printf("pk_a = %B\n", pk_a);
  //element_printf("pk_b = %B\n", pk_b);
  end_t = clock();
  total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
  printf("kgen %f\n", total_t  );

  //element_printf("pp.g = %B\n", g);
  start_t = clock();
  //printf("dpk(pk)\n");
  element_random(ske_a);
  element_random(ske_b);
  element_pow_zn(pke_a, g, ske_a);
  element_pow_zn(pke_b, g, ske_b);
  //element_printf("ske_a = %B\n", ske_a);
  //element_printf("ske_b = %B\n", ske_b);
  //element_printf("pke_a = %B\n", pke_a);
  //element_printf("pke_b = %B\n", pke_b);
  
  element_t dpk_phi_exp, dpk_phi_e;
  element_init_Zr(dpk_phi_exp, pairing);
  element_init_GT(dpk_phi_e, pairing);
  element_mul(dpk_phi_exp, ske_a, ske_b);
  element_pairing(dpk_phi_e, pk_a, pk_b);
  element_pow_zn(dpk_phi, dpk_phi_e, dpk_phi_exp);
  //element_printf("dpk.phi(ske, pk) = %B\n", dpk_phi);
  
  
  element_to_bytes(dpk_phi_bytes, dpk_phi);
  crypto_kdf_derive_from_key(dpk_kcred, sizeof dpk_kcred, 1, KDF1, dpk_phi_bytes);
  sodium_bin2hex(kdf_out_hex, sizeof kdf_out_hex, dpk_kcred, sizeof dpk_kcred);
 // printf("dpk.kcred = %s\n", kdf_out_hex);

  crypto_kdf_derive_from_key(dpk_kmac, sizeof dpk_kmac, 2, KDF2, (const unsigned char *) dpk_phi);/*
  sodium_bin2hex(kdf_out_hex, sizeof kdf_out_hex, dpk_kmac, sizeof dpk_kmac);
  printf("dpk.kmac = %s\n", kdf_out_hex);*/
  
  element_from_hash(dpk_kcred_g, dpk_kcred, sizeof dpk_kcred);
  element_pow_zn(pkp_a, g, dpk_kcred_g);
  element_add(pkp_a, pkp_a, pk_a);
  //element_printf("pkp_a = %B\n", pkp_a);
  element_pow_zn(pkp_b, g, dpk_kcred_g);
  element_add(pkp_b, pkp_b, pk_b);
  //element_printf("pkp_b = %B\n", pkp_b);
  end_t = clock();
  total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
  printf("dpk %f\n", total_t  );
  //printf("Skipping MAC for now\n");
    start_t = clock();
  //printf("dsk(sk,pke)\n"); 
  element_t dsk_phi_exp, dsk_phi_e;
  element_init_Zr(dsk_phi_exp, pairing);
  element_init_GT(dsk_phi_e, pairing);
  element_mul(dsk_phi_exp, sk_a, sk_b);
  element_pairing(dsk_phi_e, pke_a, pke_b);
  element_pow_zn(dsk_phi, dsk_phi_e, dsk_phi_exp);
  //element_printf("dsk.phi(sk, pke) = %B\n", dsk_phi);
  
  element_to_bytes(dsk_phi_bytes, dsk_phi);
  crypto_kdf_derive_from_key(dsk_kcred, sizeof dsk_kcred, 1, KDF1, dsk_phi_bytes);
  sodium_bin2hex(kdf_out_hex, sizeof kdf_out_hex, dsk_kcred, sizeof dsk_kcred);
  //printf("dsk.kcred = %s\n", kdf_out_hex);

  

  crypto_kdf_derive_from_key(dpk_kmac, sizeof dpk_kmac, 2, KDF2, (const unsigned char *) dpk_phi);/*
  sodium_bin2hex(kdf_out_hex, sizeof kdf_out_hex, dpk_kcred, sizeof dpk_kcred);
  printf("dpk.kmac = %s\n", kdf_out_hex);*/
  
  element_from_hash(dsk_kcred_g, dsk_kcred, sizeof dsk_kcred);
  element_add(skp_a, dsk_kcred_g, sk_a);
  element_add(skp_b, dsk_kcred_g, sk_b);
  //element_printf("skp_a = %B\n", skp_a);
  //element_printf("skp_b = %B\n", skp_b);
  
    end_t = clock();
  total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
  printf("dsk %f\n", total_t  );
    start_t = clock();

  element_pow_zn(pkp_rec_a, g, skp_a);
  element_pow_zn(pkp_rec_b, g, skp_b);
  //element_printf("pkp_a = %B\n", pkp_a);
  //element_printf("pkp_b = %B\n", pkp_b);
  //element_printf("pkp_rec_a = %B\n", pkp_rec_a);
  //element_printf("pkp_rec_b = %B\n", pkp_rec_b);
  //printf("%i\n", element_cmp(pkp_rec_a, pkp_a));
  //printf("%i\n", element_cmp(pkp_rec_b, pkp_b));
      end_t = clock();
  total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC;
  printf("check %f\n", total_t  );
return 0;

}
